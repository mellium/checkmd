# checkmd

[![Issue Tracker][badge]](https://mellium.im/issue)
[![Docs](https://pkg.go.dev/badge/mellium.im/checkmd)](https://pkg.go.dev/mellium.im/checkmd)
[![Chat](https://img.shields.io/badge/XMPP-users@mellium.chat-orange.svg)](https://mellium.chat)
[![License](https://img.shields.io/badge/license-FreeBSD-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

<a href="https://opencollective.com/mellium" alt="Donate on Open Collective"><img src="https://opencollective.com/mellium/donate/button@2x.png?color=blue" width="200"/></a>

The `checkmd` tool parses Markdown files and reports common linter issues.
It is mainly used to validate the documentation for [`mellium.im/xmpp`].

If you'd like to contribute to the project, see [CONTRIBUTING.md].

## License

The package may be used under the terms of the BSD 2-Clause License a copy of
which may be found in the file "[LICENSE]".

Unless you explicitly state otherwise, any contribution submitted for inclusion
in the work by you shall be licensed as above, without any additional terms or
conditions.


[badge]: https://img.shields.io/badge/style-mellium%2fxmpp-green.svg?longCache=true&style=popout-square&label=issues
[`mellium.im/xmpp`]: https://mellium.im/xmpp/
[CONTRIBUTING.md]: https://mellium.im/docs/CONTRIBUTING
[LICENSE]: https://codeberg.org/mellium/xmpp/src/branch/main/LICENSE
